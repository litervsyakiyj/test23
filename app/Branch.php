<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = "branch";
	 
    //
    protected $fillable = [
        'name', 'address', 'firm_id'
    ];
	
	public function firms() { 
		return $this->belongsToMany('App\Firm');
	}
}
