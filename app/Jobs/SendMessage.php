<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Mail;

class SendMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $message;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        //
		$this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		 
			Mail::send('emails.test', array('key' => 'value'), function($message) 
			{
				$message->to('vsyakiyj@mail.ru', 'Джон Смит')->subject('Привет!');
			}); 
			
			/*
			Mail::queue("emails.test", [], function($message) { 
				$message->to("someone@somedomain.com"); 
				$message->subject('Status has been updated'); 
			}); 
			*/
			 
		Log::info($this->message);

    }
}
