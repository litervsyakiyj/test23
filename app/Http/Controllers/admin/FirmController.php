<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Firm;
use App\Branch;
use App\Employee;

class FirmController extends Controller
{ 
    public function new_firm() {	 
        $firm_relation = Branch::leftJoin('firm', function($join) {
			$join->on('branch.firm_id', '=', 'firm.id');
		})
		->where('status', 1)
		->get(array('branch.*', 'firm.phone')); 
		 
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> refs/remotes/origin/master
        $employee_relation = Employee::leftJoin('employee_relations', function($join) {
			$join->on('employees.id', '=', 'employee_relations.employee_id');
		})->leftJoin('firm', function($join) {
			$join->on('employee_relations.firm_id', '=', 'firm.id');
		})->leftJoin('branch', function($join) {
			$join->on('employee_relations.branch_id', '=', 'branch.id');
		})
		->where('status', 1)
		->get(array('employees.*', 'branch.name as branch_name', 'firm.phone')); 
		  
		$firms = Firm::all();
		$branchs = Branch::all();
		 			
		return view('admin.new_firm', compact('firm_relation', 'employee_relation', 'firms', 'branchs'));
<<<<<<< HEAD
=======
=======
        $employee_relation = Employee::leftJoin('firm', function($join) {
			$join->on('employees.firm_id', '=', 'firm.id');
		})->leftJoin('branch', function($join) {
			$join->on('branch.firm_id', '=', 'branch.id');
		})
		->where('status', 1)
		->get(array('branch.*', 'firm.phone')); 
		
		$firms = Firm::all();
		$branchs = Branch::all();
		 			
		return view('admin.new_firm', compact('firm_relation', 'firms', 'branchs'));
>>>>>>> 58907fd9db8686ae49d118bc80188340060e7326
>>>>>>> refs/remotes/origin/master
	}
	
    public function edit_firm($id) {		
        $firm = Firm::find($id);
		
        return view('admin.edit_firm', compact('firm'));
	} 
	
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> refs/remotes/origin/master
    public function edit_branch($id) {		
        $firm = Branch::find($id);
		
        return view('admin.edit_branch', compact('firm'));
	} 
	
    public function branch_for_firm($id) {  
		
        $branch = Branch::leftJoin('firm', function($join) {
			$join->on('branch.firm_id', '=', 'firm.id');
		})
		->where('firm.id', $id)
		->get(array('branch.id', 'branch.name as text')); 
		
        return response()->json($branch);
	}
<<<<<<< HEAD
=======
=======
>>>>>>> 58907fd9db8686ae49d118bc80188340060e7326
>>>>>>> refs/remotes/origin/master
	
    public function create(Request $request) {
		$input = $request->all();  
  
		$input['status'] = 0;
		$input['description'] = json_encode($input['description']);

        $firm = Firm::create($input);
		
		//if($firm) \App\Jobs\SendMessage::dispatchNow("CREATE FIRM");
		if($firm) \App\Jobs\SendMessage::dispatch("CREATE FIRM");
		
		return redirect()->back();
<<<<<<< HEAD
	}  
	
    public function create2(Request $request) { 
			$input = $request->all();  
	 
        try {
			$firm = Branch::create($input);
        } catch (\Exception $e) { 
			return redirect()->action('admin\FirmController@new_firm');
        } 
		
        //$firm = Branch::create($input);
=======
<<<<<<< HEAD
	}  
	
    public function create2(Request $request) {
		$input = $request->all();  
   
        $firm = Branch::create($input);
>>>>>>> refs/remotes/origin/master
		 
		if($firm) \App\Jobs\SendMessage::dispatch("CREATE BRANCH");
		
		return redirect()->back();
<<<<<<< HEAD
=======
=======
>>>>>>> 58907fd9db8686ae49d118bc80188340060e7326
>>>>>>> refs/remotes/origin/master
	} 
	
    public function update($id, Request $request) { 
		$input = $request->all();  
	
        $firm = Firm::where('id', $id)->first();
        $firm->update($input);
		
		return redirect()->back();
	} 
	
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> refs/remotes/origin/master
    public function update2($id, Request $request) { 
		$input = $request->all();  
	
        $firm = Branch::where('id', $id)->first();
        $firm->update($input);
		
		return redirect()->back();
	} 
	
<<<<<<< HEAD
=======
=======
>>>>>>> 58907fd9db8686ae49d118bc80188340060e7326
>>>>>>> refs/remotes/origin/master
    public function destroy($id) {
        $project = Firm::withTrashed()->where('id',$id)->first();
        
        $project->forceDelete();
    
        return redirect()->route('admin.papers.index');
    }
}
