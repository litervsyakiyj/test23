<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Firm;

class FirmController extends Controller
{
    //
    public function all(Request $request)
    {  
        $firm = Firm::where('status', 1)->get();
	 
        return response()->json($firm);
	}
}
