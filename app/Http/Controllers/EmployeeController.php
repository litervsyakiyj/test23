<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee; 
use App\EmployeeRelation; 

class EmployeeController extends Controller
{
    //
    public function all(Request $request)
    {  
        $employee = Employee::find(1);
	 
		$employee_relation = $employee->employee_relation;
		 
		return response()->json($employee_relation);
	}
}
