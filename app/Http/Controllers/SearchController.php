<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Firm;
use App\Branch;
use App\Employee;

class SearchController extends Controller
{
    //
    public function index(Request $request) {
		$input = $request->all();  

	/*	
	$match = "MATCH(`name`, `description`) AGAINST (?)"; 
	$firms = Firm::whereRaw($match, 'test')->get();
	*/ 

	$part = explode(' ', $input['q']);
	 
	$firms = Employee::where(function($query) use ($part){  
		
		foreach($part as $p) {
         $query->where('name', 'LIKE', '%'.$p.'%');
        }
		
         })->get(); 
	
	return response()->json($firms);
		
	}
}
