<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Firm extends Model
{
    protected $table = "firm";
	 
    //
    protected $fillable = [
        'name', 'phone', 'status', 'description'
    ];
	 
	public function firm_relation() {  
		return $this->hasMany('App\Branch', 'firm_id', 'id');
	}
}
